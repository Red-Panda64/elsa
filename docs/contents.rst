.. toctree::
   :caption: elsa Guides
   :maxdepth: 2

   guides/quickstart-cxx.rst
   guides/python_guide/index
   guides/tomo/index

.. toctree::
   :caption: C++ API Reference
   :maxdepth: 2

   modules/core
   modules/storage
   modules/logging
   modules/io
   modules/operators
   modules/functionals
   modules/solvers
   modules/projectors
   modules/projectors_cuda
   modules/proximal_operators
   modules/generators
