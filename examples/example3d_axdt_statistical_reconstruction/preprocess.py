import numpy as np
import os
import scipy.ndimage.filters as snf
import h5py
import pyelsa as elsa


def binning_dc(data, binning_fac):
    desc = data.getDataDescriptor()
    data_cp = np.array(data).copy()

    data_cp = data_cp.reshape(data_cp.shape[0] // binning_fac, binning_fac,
                              data_cp.shape[1] // binning_fac, binning_fac,
                              data_cp.shape[2]
                              ).mean(3).mean(1)

    new_spacing = desc.getSpacingPerDimension() * binning_fac
    new_spacing[-1] = 1
    desc_cp = elsa.VolumeDescriptor([desc.getNumberOfCoefficientsPerDimension()[0] // binning_fac,
                                     desc.getNumberOfCoefficientsPerDimension()[1] // binning_fac,
                                     desc.getNumberOfCoefficientsPerDimension()[2]],
                                    new_spacing
                                    )
    return elsa.DataContainer(desc_cp, data_cp.swapaxes(0, 2).flatten())


def read_img(file_name, field_name='raw_data'):
    f1 = h5py.File(file_name, 'r')
    data = np.array(f1.get(field_name))
    f1.close()
    return data


def read_imgs(path, file_list, field_name='raw_data'):
    first_name = file_list[0]
    first_img = read_img(os.path.join(path, first_name), field_name)
    imgs = np.empty([len(file_list),
                     first_img.shape[0],
                     first_img.shape[1]]
                    )

    for i, filename in enumerate(file_list):
        imgs[i] = read_img(os.path.join(path, filename), field_name)

    return imgs


def get_outer_img_folder(path, id):
    series_order = id // 1000
    subdir = "S" + str(series_order).zfill(2) + str(0).zfill(3) + "-" \
             + str(series_order).zfill(2) + str(999)
    return os.path.join(path, subdir)


def get_inner_img_folder(path, id):
    outer_dir = get_outer_img_folder(path, id)
    subdir = "S" + str(id).zfill(5)
    return os.path.join(outer_dir, subdir)


def median_filter(data, size=None):
    if size is None:
        size = [3, 3]

    if len(data.shape) == 3:
        mean = np.mean(data, axis=0)
        filtered = snf.median_filter(mean, size)
        diff = mean - filtered
        return data - diff

    else:
        assert len(data.shape) == 2
        return snf.median_filter(data, size)


def trim_with_ff(a_r, b_r, a_s, b_s):
    idx = a_s / a_r > 1
    a_s[idx] = a_r[idx]

    idx = b_s / a_s / (b_r / a_r) > 1
    b_s[idx] = b_r[idx] / a_r[idx] * a_s[idx]
    return a_s, b_s


def trim(a, b, eps=1.0):
    idx = a < eps
    a[idx] = eps

    idx = b < eps
    b[idx] = eps

    idx = b > a
    b[idx] = a[idx]

    return a, b


def fft(data, n):
    fftdata = np.fft.rfft(data, axis=0, n=n)

    a = np.abs(fftdata[0])
    b = 2 * np.abs(fftdata[1])
    a, b = trim(a, b)
    return a, b


def shift_best(im1, im2, axis):
    im1 = im1.sum(axis=1 - axis).flatten()
    im2 = im2.sum(axis=1 - axis).flatten()
    size = im1.shape[0]

    min_diff = 1e20
    min_idx = 0

    for idx in range(int(-0.1 * size), 0):
        diff = np.mean(np.square(im1[:size + idx] - im2[-idx:]))
        if diff < min_diff:
            min_diff = diff
            min_idx = idx

    for idx in range(0, int(0.1 * size)):
        diff = np.mean(np.square(im1[idx:] - im2[:size - idx]))
        if diff < min_diff:
            min_diff = diff
            min_idx = idx

    if min_idx >= 0:
        ret = min_idx - 1 + int(0.5 * (size - min_idx) + 0.5)
    else:
        ret = int(0.5 * (size + min_idx) - 0.5)
    return ret


def find_center(im1, im2, axis=1):
    if axis == 1:
        center = shift_best(im1, np.fliplr(im2), axis)
    else:
        assert axis == 0
        center = shift_best(im1, np.flipud(im2), axis)
    return center


def distance_square(i, j, oi, oj):
    return (i - oi) * (i - oi) + (j - oj) * (j - oj)


def round_mask(shape, r):
    mask = np.ones(shape)
    center = np.array(shape) // 2
    for i in range(shape[0]):
        for j in range(shape[1]):
            if distance_square(i, j, center[0], center[1]) > r * r:
                mask[i, j] = 0
    return mask


def apply_mask(mask, data):
    return 1 - mask + mask * data


##
# IMPORTANT: the following are dataset-dependent parameters - change them when necessary
##

path_to_data = '/path/to/dataset'
desired_size_after_crop = 640
spacing = 0.127
binning_factor = 4
n = 8  # stepping count
begin_id = 53378  # id of the first image in the dataset
theta_begin = -90.
theta_end = 90.
theta_cnt = 4
psi_begin = -40.
psi_end = 40.
psi_cnt = 5
phi_begin = 0.
phi_end = 360.
phi_cnt = 60
cnt_inner = phi_cnt
cnt_outer = psi_cnt * theta_cnt

# probe one image to get the size
_img_folder = get_inner_img_folder(path_to_data, begin_id)
_img_name = os.listdir(_img_folder)[0]
_img = read_img(os.path.join(_img_folder, _img_name))
size_x = _img.shape[0]
size_y = _img.shape[1]

radius = min(size_x, size_y) * 0.35
mask = round_mask([size_x, size_y], radius)

ffa_ = np.ndarray([size_x, size_y, cnt_inner * cnt_outer])
ffb_ = np.ndarray([size_x, size_y, cnt_inner * cnt_outer])
a_ = np.ndarray([size_x, size_y, cnt_inner * cnt_outer])
b_ = np.ndarray([size_x, size_y, cnt_inner * cnt_outer])

outer_dir = get_outer_img_folder(path_to_data, begin_id)
outer_folders = os.listdir(outer_dir)
assert len(outer_folders) == cnt_outer * 2 + 6  # This might vary across different dataset

##
# perform fft, get a, b, ffa, ffb
##

print('Step 1: fft')

for iter_outer in range(cnt_outer):
    print('------outer loop, progress: {} / {}'.format(iter_outer, cnt_outer))
    flat_path = get_inner_img_folder(path_to_data, begin_id + 2 * iter_outer)
    data_path = get_inner_img_folder(path_to_data, begin_id + 2 * iter_outer + 1)

    flat_list = os.listdir(flat_path)
    flat_list = sorted(flat_list)
    data_list = os.listdir(data_path)
    data_list = sorted(data_list)

    ff = read_imgs(flat_path, flat_list[:-1])  # The last stepping is redundant (and not counted into n)
    a_r, b_r = fft(ff, n)

    for iter_inner in range(cnt_inner):
        print('---inner loop, progress: {} / {}'.format(iter_inner, cnt_inner))
        begin_batch = (n + 1) * iter_inner
        data = read_imgs(data_path, data_list[begin_batch: begin_batch + n])
        a_s, b_s = fft(data, n)
        a_s, b_s = trim_with_ff(a_r, b_r, a_s, b_s)
        a_s = apply_mask(mask, a_s)
        b_s = apply_mask(mask, b_s)
        a_r = apply_mask(mask, a_r)
        b_r = apply_mask(mask, b_r)

        ffa_[:, :, iter_outer * cnt_inner + iter_inner] = a_r
        ffb_[:, :, iter_outer * cnt_inner + iter_inner] = b_r
        a_[:, :, iter_outer * cnt_inner + iter_inner] = a_s
        b_[:, :, iter_outer * cnt_inner + iter_inner] = b_s


##
# construct the angle csv
##

print('Step 2: angles')

angles_csv = np.zeros((cnt_inner * cnt_outer, 4))
angles_csv[:, 0] = np.arange(1, cnt_inner * cnt_outer + 1)

step = (psi_end - psi_begin) / (psi_cnt - 1)
begin = psi_begin
end = psi_end + 0.5 * step
angles_csv[:, 2] = np.repeat(np.arange(begin, end, step), phi_cnt * theta_cnt)

step = (theta_end - theta_begin) / theta_cnt
begin = theta_begin
end = theta_end
angles_csv[:, 1] = np.tile(np.repeat(np.arange(begin, end, step), phi_cnt), (psi_cnt, 1)).flatten()

step = (phi_end - phi_begin) / (phi_cnt - 1)
begin = phi_begin
end = phi_end + 0.5 * step
angles_csv[:, 3] = np.tile(np.arange(begin, end, step), (cnt_outer, 1)).flatten()

# angles_csv[-4, 1:] = [0, 0, 0]
# angles_csv[-3, 1:] = [0, 0, 180]
# angles_csv[-2, 1:] = [0, 90, 0]
# angles_csv[-1, 1:] = [0, 90, 180]

##
# find center of rotation and crop
##

print('Step 3: find center and crop')

# For some reason, theta is flipped in dataset Crossed Sticks 2
angles_csv[:, 1] = angles_csv[:, 1] * -1

center_of_rotation = np.zeros([2])

# Get registration projections

# IMPORTANT: the registration images' IDs are also dataset-dependent, modify them accordingly.

# Registration imgs (phi_0_theta_0) and (phi_180_theta_0)
# in dataset Crossed Sticks 2 are broken, so hand-pick similar ones for registration
amp_reg = np.ndarray([size_x, size_y, 4])
amp_reg[:, :, 0] = a_[:, :, 600] / ffa_[:, :, 600]
amp_reg[:, :, 1] = a_[:, :, 630] / ffa_[:, :, 630]

# Get the other two reg imgs
flat_path = get_inner_img_folder(path_to_data, begin_id + 2 * cnt_outer + 1)
flat_list = os.listdir(flat_path)
flat_list = sorted(flat_list)
ff = read_imgs(flat_path, flat_list[:-1])
a_r, b_r = fft(ff, n)
for i in range(2):
    data_path = get_inner_img_folder(path_to_data, begin_id + 2 * cnt_outer + 4 + i)
    data_list = os.listdir(data_path)
    data_list = sorted(data_list)

    data = read_imgs(data_path, data_list[:n])
    a_s, b_s = fft(data, n)
    a_s, b_s = trim_with_ff(a_r, b_r, a_s, b_s)

    amp_reg[:, :, 2 + i] = a_s / a_r

desc = elsa.VolumeDescriptor(amp_reg.shape)
elsa.EDF.write(elsa.DataContainer(desc, amp_reg.swapaxes(0, 2).flatten()), "./amp_reg.edf")

# Find horizontal center of rotation
center_of_rotation[1] = find_center(amp_reg[:, :, 0], amp_reg[:, :, 1], axis=1)
# Find vertical center of rotation
center_of_rotation[0] = find_center(amp_reg[:, :, 2], amp_reg[:, :, 3], axis=0)

print('Center of rotation: {}, {}'.format(center_of_rotation[0], center_of_rotation[1]))

x1 = int(center_of_rotation[0] - desired_size_after_crop / 2 + .5)
x2 = int(center_of_rotation[0] + desired_size_after_crop / 2 + .5)
y1 = int(center_of_rotation[1] - desired_size_after_crop / 2 + .5)
y2 = int(center_of_rotation[1] + desired_size_after_crop / 2 + .5)

final_ffa = ffa_[x1:x2, y1:y2, :]
final_ffb = ffb_[x1:x2, y1:y2, :]
final_a = a_[x1:x2, y1:y2, :]
final_b = b_[x1:x2, y1:y2, :]

##
# output
##

print('Step 4: save results')

filename = './angles.csv'
np.savetxt(filename, angles_csv, delimiter=" ", fmt="%.5f")

desc = elsa.VolumeDescriptor(final_ffa.shape, [spacing, spacing, 1])

elsa.EDF.write(binning_dc(elsa.DataContainer(desc, final_ffa.swapaxes(0, 2).flatten()), binning_factor),
               "./ffa.edf")
elsa.EDF.write(binning_dc(elsa.DataContainer(desc, final_ffb.swapaxes(0, 2).flatten()), binning_factor),
               "./ffb.edf")
elsa.EDF.write(binning_dc(elsa.DataContainer(desc, final_a.swapaxes(0, 2).flatten()), binning_factor),
               "./a.edf")
elsa.EDF.write(binning_dc(elsa.DataContainer(desc, final_b.swapaxes(0, 2).flatten()), binning_factor),
               "./b.edf")
