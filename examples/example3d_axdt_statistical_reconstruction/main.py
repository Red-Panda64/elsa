import argparse
from stat_recon import StatRecon

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('filename')
    parser.add_argument('output_folder')
    parser.add_argument('recon_type')
    parser.add_argument('-f', '--flat_field',
                        action='store_true')
    parser.add_argument('-b', '--binning_factor',
                        type=int)
    parser.add_argument('-p', '--period',
                        type=int)
    parser.add_argument('-e', '--eps',
                        type=float)
    parser.add_argument('-i', '--iteration',
                        type=int)
    parser.add_argument('-v', '--verbose',
                        action='store_true')

    args = parser.parse_args()
    assert args.recon_type in ["glogd", "gd", "gb", "rb"]
    if args.recon_type in ["gb", "rb"]:
        assert args.flat_field

    print("Parameters: info_file: {}, output_loc: {}, binning_factor: {}, "
          "recon_type: {}, is_flat_field_input: {}, "
          "grating_period: {}, eps: {}, iter: {}".format(
            args.filename, args.output_folder, args.binning_factor, args.recon_type,
            args.flat_field, args.period, args.eps, args.iteration
            )
          )

    recon = StatRecon(args.filename, args.output_folder,
                      args.binning_factor, args.recon_type,
                      args.flat_field, args.period,
                      args.verbose)

    recon.reconstruct(args.eps, args.iteration)
