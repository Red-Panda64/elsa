#pragma once

#include "LineSearchMethod.h"

namespace elsa
{
    /**
     * @brief Strong Wolfe Condition
     *
     * StrongWolfeCondition is a line search method attempting to find a step size
     * @f$\alpha@f$ that satisfies the sufficient decrease and curvature condition while making sure
     * that the step size @f$\alpha@f$ is not too short using the following inequalities:
     *
     * @f[
     * f(x_i+\alpha d_i) \le f(x_i) + c_1 \alpha \nabla f_i^T d_i \\
     * |\nabla f(x_i + \alpha d_i)^T d_i| \le c_2 |\nabla f_i^T d_i|
     * @f]
     *
     * where @f$f: \mathbb{R}^n \to \mathbb{R}@f$ is differentiable,
     * @f$\nabla f_i: \mathbb{R}^n is the gradient of f at x_i@f$,
     * @f$ d_i: \mathbb{R}^n is the search direction @f$,
     * @f$ c1: is a constant affecting the sufficient decrease condition @f$, and
     * @f$ c: is a constant affecting the curvature condition@f$.
     *
     * References:
     * - See Wright and Nocedal, ‘Numerical Optimization’, 2nd Edition, 2006, pp. 33-36.
     *
     * @author
     * - Said Alghabra - initial code
     *
     * @tparam data_t data type for the domain and range of the problem, defaulting to real_t
     */
    template <typename data_t = real_t>
    class StrongWolfeCondition : public LineSearchMethod<data_t>
    {
    public:
        StrongWolfeCondition(const Functional<data_t>& problem, data_t amax = 10, data_t c1 = 1e-4,
                             data_t c2 = 0.9, index_t max_iterations = 10);
        ~StrongWolfeCondition() override = default;
        data_t solve(DataContainer<data_t> xi, DataContainer<data_t> di) override;

    private:
        // largest allowed step size
        data_t _amax;

        // parameter affecting the sufficient decrease condition
        data_t _c1;

        // parameter affecting the curvature condition
        data_t _c2;
        data_t _zoom(data_t a_lo, data_t a_hi, data_t f_lo, data_t f_hi, data_t f0, data_t der_f_lo,
                     data_t der_f0, const DataContainer<data_t>& xi,
                     const DataContainer<data_t>& di, index_t max_iterations = 10);

        /// implement the polymorphic clone operation
        StrongWolfeCondition<data_t>* cloneImpl() const override;

        /// implement the polymorphic comparison operation
        bool isEqual(const LineSearchMethod<data_t>& other) const override;
    };
} // namespace elsa
