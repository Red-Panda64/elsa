#include "BarzilaiBorwein.h"
namespace elsa
{
    template <typename data_t>
    BarzilaiBorwein<data_t>::BarzilaiBorwein(const Functional<data_t>& problem, uint32_t m,
                                             data_t gamma, data_t sigma1, data_t sigma2,
                                             data_t epsilon, index_t max_iterations)
        : LineSearchMethod<data_t>(problem, max_iterations),
          _m(m),
          _gamma(gamma),
          _sigma1(sigma1),
          _sigma2(sigma2),
          _epsilon(epsilon),
          _gi_prev(DataContainer<data_t>(this->_problem->getDomainDescriptor()))
    {
        // sanity checks
        if (m < 0)
            throw InvalidArgumentError("BarzilaiBorwein: m has to be a non-negative integer");
        if (gamma <= 0 or gamma >= 1)
            throw InvalidArgumentError("BarzilaiBorwein: gamma has to be in the range (0,1)");
        if (sigma1 <= 0 or sigma1 >= sigma2 or sigma1 >= 1)
            throw InvalidArgumentError(
                "BarzilaiBorwein: sigma1 has to satisfy 0 < sigma1 < sigma2 < 1");
        if (sigma2 <= 0 or sigma1 >= sigma2 or sigma2 >= 1)
            throw InvalidArgumentError(
                "BarzilaiBorwein: sigma2 has to satisfy 0 < sigma1 < sigma2 < 1");
        if (epsilon <= 0)
            throw InvalidArgumentError("BarzilaiBorwein: epsilon has to be in the range (0,1)");
        _invepsilon = 1 / epsilon;
        _function_vals.reserve(m);
        _li_prev = 1;
        _iter = 0;
    }
    template <typename data_t>
    data_t BarzilaiBorwein<data_t>::solve(DataContainer<data_t> xi, DataContainer<data_t> di)
    {
        auto derphi = di.dot(di);
        if (_iter == 0) {
            _gi_prev = -di;
            if (_m > 0) {
                _function_vals.push_back(this->_problem->evaluate(xi));
            }
            _derphi_prev = derphi;
        }
        auto ai = -_gi_prev.dot(-di - _gi_prev) / (_li_prev * _derphi_prev);
        if (ai <= _epsilon or ai >= _invepsilon) {
            auto g_norm = std::sqrt(derphi);
            if (g_norm > 1) {
                ai = 1;
            } else if (g_norm >= 1e-5 and g_norm <= 1) {
                ai = 1 / g_norm;
            } else {
                ai = 1e5;
            }
        }
        auto li = 1 / ai;
        data_t max_prev_f;
        if (_m > 0) {
            max_prev_f = *std::max_element(_function_vals.begin(), _function_vals.end());
        }
        data_t fi;
        ++_iter;
        for (index_t i = 0; i < this->_max_iterations and _m > 0; ++i) {
            fi = this->_problem->evaluate(xi + li * di);
            if (fi <= max_prev_f - _gamma * li * derphi) {
                break;
            } else {
                li = (_sigma2 - _sigma1) / 2 * li;
            }
        }
        _li_prev = li;
        if (_m > 0) {

            if (_iter < _m) {
                _function_vals.push_back(fi);
            } else {
                _function_vals[_iter % _m] = fi;
            }
        }
        _gi_prev = -di;
        _derphi_prev = derphi;
        return li;
    }

    template <typename data_t>
    BarzilaiBorwein<data_t>* BarzilaiBorwein<data_t>::cloneImpl() const
    {
        return new BarzilaiBorwein(*this->_problem, _m, _gamma, _sigma1, _sigma2, _epsilon,
                                   this->_max_iterations);
    }

    template <typename data_t>
    bool BarzilaiBorwein<data_t>::isEqual(const LineSearchMethod<data_t>& other) const
    {
        auto otherBB = downcast_safe<BarzilaiBorwein<data_t>>(&other);
        if (!otherBB)
            return false;

        return (_m == otherBB->_m and _gamma == otherBB->_gamma and _sigma1 == otherBB->_sigma1
                and _sigma2 == otherBB->_sigma2 and _epsilon == otherBB->_epsilon);
    }
    // ------------------------------------------
    // explicit template instantiation
    template class BarzilaiBorwein<float>;
    template class BarzilaiBorwein<double>;
} // namespace elsa
