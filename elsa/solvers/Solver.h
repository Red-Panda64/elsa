#pragma once

#include "DataDescriptor.h"
#include "elsaDefines.h"
#include "DataContainer.h"
#include "Cloneable.h"
#include <chrono>
#include <optional>

namespace elsa
{

    /**
     * @brief Base class representing a solver for an optimization problem.
     *
     * This class represents abstract (typically iterative) solvers acting on optimization problems.
     *
     * @author
     * - Matthias Wieczorek - initial code
     * - Maximilian Hornung - modularization
     * - Tobias Lasser - rewrite, modernization
     *
     * @tparam data_t data type for the domain and range of the problem, defaulting to real_t
     */
    template <typename data_t = real_t>
    class Solver : public Cloneable<Solver<data_t>>
    {
    public:
        /// Scalar alias
        using Scalar = data_t;

        Solver() = default;

        /// default destructor
        ~Solver() override = default;

        /**
         * @brief Solve the optimization problem from a zero starting point. This
         * will setup the iterative algorithm and then run the algorithm for
         * the specified number of iterations (assuming the algorithm doesn't convergence before).
         *
         * @param[in] iterations number of iterations to execute
         * @param[in] x0 optional initial solution, initial solution set to zero if not present
         *
         * @returns the current solution (after solving)
         */
        virtual DataContainer<data_t> solve(index_t iterations,
                                            std::optional<DataContainer<data_t>> x0 = std::nullopt);

        /**
         * @brief Setup and reset the solver. This function should set all values,
         * vectors and temporaries necessary to run the iterative reconstruction
         * algorithm.
         *
         * @param[in] x optional to initial solution, if optional is empty, some defaulted (e.g. 0
         * filled) initial solution should be returned
         *
         * @returns initial solution
         */
        virtual DataContainer<data_t> setup(std::optional<DataContainer<data_t>> x);

        /**
         * @brief Perform a single step of the iterative reconstruction algorithm.
         *
         * @param[in] x the current solution
         *
         * @returns the updated solution estimate
         */
        virtual DataContainer<data_t> step(DataContainer<data_t> x);

        /**
         * @brief Run the iterative reconstruction algorithm for the
         * given number of iterations on the initial starting point. This fun
         *
         * @param[in] iterations the number of iterations to execute
         * @param[in] x0 the initial starting point of the run
         * @param[in] show if the algorithm should print
         *
         * @returns the current solution estimate
         */
        virtual DataContainer<data_t> run(index_t iterations, const DataContainer<data_t>& x0,
                                          bool show = false);

        /**
         * @brief Function to determine when to stop. This function should implement
         * algorithm specific stopping criterions.
         *
         * @returns true if the algorithm should stop
         */
        virtual bool shouldStop() const;

        /**
         * @brief Format the header string for the iterative reconstruction algorithm
         */
        virtual std::string formatHeader() const;

        /**
         * @brief Print the header of the iterative reconstruction algorithm
         */
        virtual void printHeader() const;

        /**
         * @brief Format the step string for the iterative reconstruction algorithm
         */
        virtual std::string formatStep(const DataContainer<data_t>& x) const;

        /**
         * @brief Print a step of the iterative reconstruction algorithm
         */
        virtual void printStep(const DataContainer<data_t>& x, index_t curiter,
                               std::chrono::duration<double> steptime,
                               std::chrono::duration<double> elapsed) const;

        /**
         * @brief Set the maximum number of iterations
         */
        void setMaxiters(index_t maxiters);

        /**
         * @brief Set the callback function
         *
         * An example usage to later plot the loss (assuming some ground truth data):
         * ```cpp
         * auto phantom = getSomePhantom();
         * auto solver = SomeSolver(...);
         *
         * // compute mean square error for each iteration
         * std::vector<float> msre;
         * solver.setCallback([&msre](const DataContainer<data_t>& x){
         *     msre.push_back(square(phantom - x).sum());
         * });
         * ```
         * Similarly from Python:
         * ```py
         * phantom = getSomePhantom()
         * solver = SomeSolver(...)
         *
         * // compute mean square error for each iteration
         * msre = []
         * solver.setCallback(lambda x: msre.append(square(phantom - x).sum()))
         * ```
         */
        void setCallback(const std::function<void(const DataContainer<data_t>&)>& callback);

        /**
         * @brief Set variable to print only every n steps of the iterative reconstruction algorithm
         */
        void printEvery(index_t printEvery);

    protected:
        /// Is the solver already configured (no need to call `setup`)
        bool configured_ = false;

        /// Logging name of the iterative reconstruction algorithm
        std::string name_ = "Solver";

    private:
        /// Current iteration
        index_t curiter_ = 0;

        /// Max iteration to run in total
        index_t maxiters_ = 10000;

        index_t printEvery_ = 1;

        /// Callback function to call each iteration, by default, do nothing
        std::function<void(const DataContainer<data_t>&)> callback_ = [](auto x) {};
    };

    /// Extract the default value from the optional if present, else create a
    /// new DataContainer with the given Descriptor and initial value
    template <class data_t>
    DataContainer<data_t> extract_or(std::optional<DataContainer<data_t>> x0,
                                     const DataDescriptor& domain,
                                     SelfType_t<data_t> val = data_t{0})
    {
        return x0.has_value() ? x0.value() : full<data_t>(domain, val);
    }
} // namespace elsa
