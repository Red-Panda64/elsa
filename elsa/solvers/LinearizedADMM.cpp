#include "LinearizedADMM.h"
#include "LinearOperator.h"
#include "ProximalOperator.h"
#include "TypeTraits.hpp"
#include "PowerIterations.h"
#include "elsaDefines.h"
#include "DataContainer.h"
#include "Logger.h"

namespace elsa
{

    template <class data_t>
    LinearizedADMM<data_t>::LinearizedADMM(const LinearOperator<data_t>& K,
                                           ProximalOperator<data_t> proxf,
                                           ProximalOperator<data_t> proxg, SelfType_t<data_t> sigma,
                                           std::optional<data_t> tau, bool computeKNorm)
        : K_(K.clone()),
          proxf_(proxf),
          proxg_(proxg),
          z_(empty<data_t>(K.getDomainDescriptor())),
          u_(empty<data_t>(K.getRangeDescriptor())),
          tmpDomain_(empty<data_t>(K.getDomainDescriptor())),
          tmpRange_(empty<data_t>(K.getRangeDescriptor())),
          sigma_(sigma)
    {
        if (sigma_ <= 0) {
            throw Error("`sigma` must be strictly positive. Got {}", sigma_);
        }

        if (computeKNorm) {
            auto Knorm = powerIterations(adjoint(K) * K);
            Logger::get("LinearizedADMM")
                ->info("Knorm: {}, sigma / || K ||_2^2: {}", Knorm, sigma_ / Knorm);

            if (tau.has_value()) {
                tau_ = tau.value();
            } else {
                tau_ = 0.95 * (sigma_ / Knorm);
                Logger::get("LinearizedADMM")->info("Set tau to sigma_ / Knorm: {}", tau_);
            }

            if (!(0 < tau_ && tau_ < sigma_ / Knorm)) {
                Logger::get("LinearizedADMM")
                    ->warn("Parameters do not satisfy: 0 < tau < sigma / ||K||_2^2 ({}, {}). Might "
                           "not "
                           "converge.",
                           tau_, sigma_ / Knorm);
            }
        }

        else if (!tau.has_value()) {
            throw Error("set `tau` or compute norm");
        }

        if (tau_ <= 0) {
            throw Error("`tau` must be strictly positive. Got {}", tau_);
        }
    }

    template <typename data_t>
    DataContainer<data_t> LinearizedADMM<data_t>::setup(std::optional<DataContainer<data_t>> x0)
    {
        auto& domain = K_->getDomainDescriptor();
        auto& range = K_->getRangeDescriptor();

        auto x = extract_or(x0, domain);

        z_ = zeros<data_t>(range);
        u_ = zeros<data_t>(range);

        // Temporary for Kx + u - z
        tmpRange_ = K_->apply(x);

        // Temporary for L^T (Lx + u - z)
        tmpDomain_ = empty<data_t>(domain);

        return x;
    }

    template <typename data_t>
    DataContainer<data_t> LinearizedADMM<data_t>::step(DataContainer<data_t> x)
    {
        // tmpRange is equal to Kx here, so we can compute:
        // tmpRange = Kx + u - z
        lincomb(1, tmpRange_, 1, u_, tmpRange_);
        lincomb(1, tmpRange_, -1, z_, tmpRange_);

        // K^T(Kx + u - z)
        K_->applyAdjoint(tmpRange_, tmpDomain_);

        // x = x^k - (tau/sigma) K^T(Kx^k + u^k - z^k)
        lincomb(1, x, -(tau_ / sigma_), tmpDomain_, x);

        // x^{k+1} = prox_{tau * f}(x)
        x = proxf_.apply(x, tau_);

        // tmp_ran = Kx^{k + 1}
        K_->apply(x, tmpRange_);

        // First part of:
        // u^{k+1} = u^k + Kx^{k+1} - z^{k+1}
        u_ += tmpRange_;

        // z^{k+1} = prox{sigma * g}(u^k + Kx^{k+1})
        z_ = proxg_.apply(u_, sigma_);

        // Second part of
        // u^{k+1} = u^k + Kx^{k+1} - z^{k+1}
        u_ -= z_;

        return x;
    }

    template <typename data_t>
    bool LinearizedADMM<data_t>::shouldStop() const
    {
        return false;
    }

    template <typename data_t>
    std::string LinearizedADMM<data_t>::formatHeader() const
    {
        return fmt::format("{:^12} | {:^12} | {:^12}", "x", "z", "u");
    }

    template <typename data_t>
    std::string LinearizedADMM<data_t>::formatStep(const DataContainer<data_t>& x) const
    {
        return fmt::format("{:>12.7} | {:>12.7} | {:>12.7}", x.l2Norm(), z_.l2Norm(), u_.l2Norm());
    }

    template <class data_t>
    LinearizedADMM<data_t>* LinearizedADMM<data_t>::cloneImpl() const
    {
        return new LinearizedADMM<data_t>(*K_, proxf_, proxg_, sigma_, tau_, false);
    }

    template <class data_t>
    bool LinearizedADMM<data_t>::isEqual(const Solver<data_t>& other) const
    {
        auto otherLADMM = downcast_safe<LinearizedADMM>(&other);
        if (!otherLADMM)
            return false;

        if (*K_ != *otherLADMM->K_)
            return false;

        if (sigma_ != otherLADMM->sigma_)
            return false;

        if (tau_ != otherLADMM->tau_)
            return false;

        return true;
    }

    // ------------------------------------------
    // explicit template instantiation
    template class LinearizedADMM<float>;
    template class LinearizedADMM<double>;
} // namespace elsa
