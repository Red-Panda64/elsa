#include "POGM.h"
#include "DataContainer.h"
#include "Error.h"
#include "Functional.h"
#include "LeastSquares.h"
#include "LinearOperator.h"
#include "LinearResidual.h"
#include "ProximalL1.h"
#include "TypeCasts.hpp"
#include "Logger.h"
#include "PowerIterations.h"

#include "WeightedLeastSquares.h"
#include "spdlog/stopwatch.h"
#include <cmath>

namespace elsa
{
    template <typename data_t>
    POGM<data_t>::POGM(const LinearOperator<data_t>& A, const DataContainer<data_t>& b,
                       const Functional<data_t>& h, std::optional<data_t> mu, data_t epsilon)
        : g_(LeastSquares<data_t>(A, b).clone()), h_(h.clone()), mu_(0), epsilon_(epsilon)
    {
        if (!h.isProxFriendly()) {
            throw Error("POGM: h must be prox friendly");
        }

        if (mu.has_value()) {
            mu_ = *mu;
        } else {
            Logger::get("PGD")->info("Computing Lipschitz constant for least squares...");
            // Chose it a little larger, to be safe
            auto L = 1.05 * powerIterations(adjoint(A) * A);
            mu_ = 1 / L;
            Logger::get("PGD")->info("Step length chosen to be: {}", mu_);
        }
    }

    template <typename data_t>
    POGM<data_t>::POGM(const LinearOperator<data_t>& A, const DataContainer<data_t>& b,
                       const DataContainer<data_t>& W, const Functional<data_t>& h,
                       std::optional<data_t> mu, data_t epsilon)
        : g_(WeightedLeastSquares<data_t>(A, b, W).clone()),
          h_(h.clone()),
          mu_(0),
          epsilon_(epsilon)
    {
        if (!h.isProxFriendly()) {
            throw Error("APGD: h must be prox friendly");
        }

        if (mu.has_value()) {
            mu_ = *mu;
        } else {
            Logger::get("PGD")->info("Computing Lipschitz constant for least squares...");
            // Chose it a little larger, to be safe
            auto L = 1.05 * powerIterations(adjoint(A) * A);
            mu_ = 1 / L;
            Logger::get("PGD")->info("Step length chosen to be: {}", mu_);
        }
    }

    template <typename data_t>
    POGM<data_t>::POGM(const Functional<data_t>& g, const Functional<data_t>& h, data_t mu,
                       data_t epsilon)
        : g_(g.clone()), h_(h.clone()), mu_(mu), epsilon_(epsilon)
    {
        if (!h.isProxFriendly()) {
            throw Error("POGM: h must be prox friendly");
        }

        if (!g.isDifferentiable()) {
            throw Error("POGM: g must be differentiable");
        }
    }

    template <typename data_t>
    auto POGM<data_t>::solve(index_t iterations, std::optional<DataContainer<data_t>> x0)
        -> DataContainer<data_t>
    {
        spdlog::stopwatch aggregate_time;

        auto x = DataContainer<data_t>(g_->getDomainDescriptor());
        if (x0.has_value()) {
            x = *x0;
        } else {
            x = 0;
        }

        auto w = x;
        auto wPrev = x;
        auto z = x;

        data_t theta = 1;
        data_t thetaPrev = 1;

        data_t gamma = 1;
        data_t gammaPrev = 1;

        auto grad = DataContainer<data_t>(g_->getDomainDescriptor());

        Logger::get("POGM")->info("| {:^6} | {:^12} | {:^12} | {:^9} |", "iter", "objective",
                                  "gradient", "elapsed");

        for (index_t iter = 0; iter < iterations; ++iter) {
            if (iter != iterations - 1) {
                // \frac{1}{2}(1 + \sqrt{4 \theta_{k-1}^2 + 1})
                theta = 0.5 * (1 + std::sqrt(4 * std::pow(thetaPrev, 2) + 1));
            } else {
                // \frac{1}{2}(1 + \sqrt{8 \theta_{k-1}^2 + 1})
                theta = 0.5 * (1 + std::sqrt(8 * std::pow(thetaPrev, 2) + 1));
            }

            gamma = mu_ * ((2 * thetaPrev) + theta - 1) / theta;

            // Compute gradient
            g_->getGradient(x, grad);

            // w = x - mu_ * grad
            lincomb(1, x, -mu_, grad, w);

            // POGM term: w_k + (\theta_{k-1} - 1) / (L * \gamma_{k-1} * \theta_k) (z_{k-1} -
            // x_{k-1}) Use the fact the our mu should be (close to) 1 / L. Start with this term to
            // reuse z.
            data_t weight3 = mu_ * ((thetaPrev - 1) / (gammaPrev * theta));
            lincomb(1, w, weight3, z, z);
            lincomb(1, z, -weight3, x, z);

            // Nesterov momentum: (\theta_{k-1} - 1) / \theta_k (w_k - w_{k-1})
            auto weight1 = (thetaPrev - 1) / theta;
            lincomb(1, z, weight1, w, z);
            lincomb(1, z, -weight1, wPrev, z);

            // OGM mementum term: (\theta_{k-1}  / \theta) (w_k - x_{k-1})
            data_t weight2 = thetaPrev / theta;
            lincomb(1, z, weight2, w, z);
            lincomb(1, z, -weight2, x, z);

            // x_{k+1} = prox_{gamma * g}(z)
            x = h_->proximal(z, gamma);

            wPrev = w;

            thetaPrev = theta;
            gammaPrev = gamma;

            if (grad.squaredL2Norm() <= epsilon_) {
                Logger::get("POGM")->info("SUCCESS: Reached convergence at {}/{} iteration",
                                          iter + 1, iterations);
                return x;
            }

            Logger::get("POGM")->info("| {:>6} | {:>12.3} | {:>12.3} | {:>8.3}s |", iter,
                                      g_->evaluate(x) + h_->evaluate(x), grad.squaredL2Norm(),
                                      aggregate_time);
        }

        Logger::get("POGM")->warn("Failed to reach convergence at {} iterations", iterations);

        return x;
    }

    template <typename data_t>
    auto POGM<data_t>::cloneImpl() const -> POGM<data_t>*
    {
        return new POGM<data_t>(*g_, *h_, mu_, epsilon_);
    }

    template <typename data_t>
    auto POGM<data_t>::isEqual(const Solver<data_t>& other) const -> bool
    {
        auto otherPOGM = downcast_safe<POGM>(&other);
        if (!otherPOGM)
            return false;

        if (mu_ != otherPOGM->mu_)
            return false;

        if (epsilon_ != otherPOGM->epsilon_)
            return false;

        return true;
    }

    // ------------------------------------------
    // explicit template instantiation
    template class POGM<float>;
    template class POGM<double>;
} // namespace elsa
