
#include <optional>

#include "DataContainer.h"
#include "Functional.h"
#include "Solver.h"
#include "LinearOperator.h"
#include "StrongTypes.h"
#include "MaybeUninitialized.hpp"
#include "ProximalOperator.h"

namespace elsa
{
    /**
     * @brief Accelerated Proximal Gradient Descent (APGD)
     *
     * APGD minimizes function of the the same for as PGD. See the documentation there.
     *
     * This class represents a APGD solver with the following steps:
     *
     *  - @f$ x_{k} = prox_h(y_k - \mu * A^T (Ay_k - b)) @f$
     *  - @f$ t_{k+1} = \frac{1 + \sqrt{1 + 4 * t_{k}^2}}{2} @f$
     *  - @f$ y_{k+1} = x_{k} + (\frac{t_{k} - 1}{t_{k+1}}) * (x_{k} - x_{k - 1}) @f$
     *
     * APGD has a worst-case complexity result of @f$ O(1/k^2) @f$.
     *
     * References:
     * http://www.cs.cmu.edu/afs/cs/Web/People/airg/readings/2012_02_21_a_fast_iterative_shrinkage-thresholding.pdf
     * https://arxiv.org/pdf/2008.02683.pdf
     *
     * @see For a more detailed discussion of the type of problem for this solver,
     * see PGD.
     *
     * @author
     * Andi Braimllari - initial code
     * David Frank - generalization to APGD
     *
     * @tparam data_t data type for the domain and range of the problem, defaulting to real_t
     */
    template <typename data_t = real_t>
    class APGD : public Solver<data_t>
    {
    public:
        /// Scalar alias
        using Scalar = typename Solver<data_t>::Scalar;

        /// Construct APGD with a least squares data fidelity term
        ///
        /// @note The step length for least squares can be chosen to be dependend on the Lipschitz
        /// constant. Compute it using `powerIterations(adjoint(A) * A)`. Depending on which
        /// literature, both \f$ \frac{2}{L} \f$ and \f$ \frac{1}{L}\f$. If mu is not given, the
        /// step length is chosen by default, the computation of the power method might be
        /// expensive.
        ///
        /// @param A the operator for the least squares data term
        /// @param b the measured data for the least squares data term
        /// @param prox the proximal operator for g
        /// @param mu the step length
        APGD(const LinearOperator<data_t>& A, const DataContainer<data_t>& b,
             const Functional<data_t>& h, std::optional<data_t> mu = std::nullopt,
             data_t epsilon = std::numeric_limits<data_t>::epsilon());

        /// Construct APGD with a weighted least squares data fidelity term
        ///
        /// @note The step length for least squares can be chosen to be dependend on the Lipschitz
        /// constant. Compute it using `powerIterations(adjoint(A) * A)`. Depending on which
        /// literature, both \f$ \frac{2}{L} \f$ and \f$ \frac{1}{L}\f$. If mu is not given, the
        /// step length is chosen by default, the computation of the power method might be
        /// expensive.
        ///
        /// @param A the operator for the least squares data term
        /// @param b the measured data for the least squares data term
        /// @param W the weights (usually `counts / I0`)
        /// @param prox the proximal operator for g
        /// @param mu the step length
        APGD(const LinearOperator<data_t>& A, const DataContainer<data_t>& b,
             const DataContainer<data_t>& W, const Functional<data_t>& h,
             std::optional<data_t> mu = std::nullopt,
             data_t epsilon = std::numeric_limits<data_t>::epsilon());

        /// Construct APGD with a given data fidelity term
        ///
        /// @param g differentiable function of the LASSO problem
        /// @param h prox friendly functional of the LASSO problem
        /// @param mu the step length
        APGD(const Functional<data_t>& g, const Functional<data_t>& h, data_t mu,
             data_t epsilon = std::numeric_limits<data_t>::epsilon());

        /// default destructor
        ~APGD() override = default;

        DataContainer<data_t> setup(std::optional<DataContainer<data_t>> x) override;

        DataContainer<data_t> step(DataContainer<data_t> x) override;

        bool shouldStop() const override;

        std::string formatHeader() const override;

        std::string formatStep(const DataContainer<data_t>& x) const override;

    protected:
        /// implement the polymorphic clone operation
        auto cloneImpl() const -> APGD<data_t>* override;

        /// implement the polymorphic comparison operation
        auto isEqual(const Solver<data_t>& other) const -> bool override;

    private:
        /// Differentiable part of the problem formulation
        std::unique_ptr<Functional<data_t>> g_;

        /// Prox-friendly part of the problem formulation
        std::unique_ptr<Functional<data_t>> h_;

        DataContainer<data_t> xPrev_;

        DataContainer<data_t> y_;

        DataContainer<data_t> z_;

        DataContainer<data_t> grad_;

        data_t tPrev_ = 1;

        /// the step size
        data_t mu_;

        /// variable affecting the stopping condition
        data_t epsilon_;
    };
} // namespace elsa
