#include "ALB.h"
#include "DataContainer.h"
#include "LinearOperator.h"
#include "TypeCasts.hpp"
#include "Logger.h"

#include "spdlog/stopwatch.h"
#include "PowerIterations.h"

namespace elsa
{
    template <typename data_t>
    ALB<data_t>::ALB(const LinearOperator<data_t>& A, const DataContainer<data_t>& b,
                     ProximalOperator<data_t> prox, data_t mu, std::optional<data_t> beta,
                     data_t epsilon)
        : A_(A.clone()), b_(b), prox_(prox), mu_(mu), epsilon_(epsilon)
    {
        if (!beta.has_value()) {

            beta_ = data_t{2} / (mu_ * powerIterations(adjoint(*A_) * (*A_)));
            Logger::get("ALB")->info("Step length is chosen to be: {:8.5}", beta_);

        } else {
            beta_ = *beta;
        }
    }

    template <typename data_t>
    auto ALB<data_t>::solve(index_t iterations, std::optional<DataContainer<data_t>> x0)
        -> DataContainer<data_t>
    {
        spdlog::stopwatch iter_time;

        auto v = DataContainer<data_t>(A_->getDomainDescriptor());
        auto x = DataContainer<data_t>(A_->getDomainDescriptor());

        auto v_prev = DataContainer<data_t>(A_->getDomainDescriptor());
        auto v_tilda = DataContainer<data_t>(A_->getDomainDescriptor());
        auto a = data_t(0);

        v = 0;
        v_tilda = 0;

        if (x0.has_value()) {
            x = *x0;
        } else {
            x = 0;
        }

        auto residual = DataContainer<data_t>(b_.getDataDescriptor());

        for (int i = 0; i < iterations; ++i) {

            v_prev = v;

            // x^{k+1} = mu * prox(v^k, 1)
            x = mu_ * prox_.apply(v_tilda, 1);

            // residual = b - Ax^{k+1}
            lincomb(1, b_, -1, A_->apply(x), residual);

            // v^{k+1} = v_tilda^k + beta * A^{*}(b - Ax^{k+1})
            lincomb(1, v_tilda, beta_, A_->applyAdjoint(residual), v);

            // a_k = (2i + 3) / (i + 3)
            a = static_cast<float>(2 * i + 3) / (i + 3);

            // v_tilda^{k+1} = a_k * v^{k+1} + (1 - a_k) * v^k
            lincomb(a, v, (1 - a), v_prev, v_tilda);

            auto error = residual.squaredL2Norm() / b_.squaredL2Norm();
            if (residual.squaredL2Norm() / b_.squaredL2Norm() <= epsilon_) {
                Logger::get("ALB")->info("SUCCESS: Reached convergence at {}/{} iteration", i + 1,
                                         iterations);
                return x;
            }

            Logger::get("ALB")->info("|iter: {:>6} | x: {:>12} | v: {:>12} | v_tilda: {:>12} | a: "
                                     "{:>12} | error: {:>12} | time: {:>8.3} |",
                                     i, x.squaredL2Norm(), v.squaredL2Norm(),
                                     v_tilda.squaredL2Norm(), a, error, iter_time);
        }

        Logger::get("ALB")->warn("Failed to reach convergence at {} iterations", iterations);

        return x;
    }

    template <typename data_t>
    auto ALB<data_t>::cloneImpl() const -> ALB<data_t>*
    {
        return new ALB<data_t>(*A_, b_, prox_, mu_, beta_, epsilon_);
    }

    template <typename data_t>
    auto ALB<data_t>::isEqual(const Solver<data_t>& other) const -> bool
    {
        auto otherAlb = downcast_safe<ALB>(&other);
        if (!otherAlb)
            return false;

        if (*A_ != *otherAlb->A_)
            return false;

        if (b_ != otherAlb->b_)
            return false;

        Logger::get("ALB")->info("beta: {}, {}", beta_, otherAlb->beta_);
        if (std::abs(beta_ - otherAlb->beta_) > 1e-5)
            return false;

        Logger::get("ALB")->info("mu: {}, {}", mu_, otherAlb->mu_);
        if (mu_ != otherAlb->mu_)
            return false;

        Logger::get("ALB")->info("epsilon: {}, {}", epsilon_, otherAlb->epsilon_);
        if (epsilon_ != otherAlb->epsilon_)
            return false;

        return true;
    }

    // ------------------------------------------
    // explicit template instantiation
    template class ALB<float>;
    template class ALB<double>;
} // namespace elsa
