#include "MixedL21Norm.h"
#include "FiniteDifferences.h"
#include "ProximalMixedL21Norm.h"
#include "BlockDescriptor.h"

namespace elsa
{
    template <typename data_t>
    MixedL21Norm<data_t>::MixedL21Norm(const DataDescriptor& domainDescriptor)
        : Functional<data_t>(domainDescriptor)
    {
    }

    template <typename data_t>
    data_t MixedL21Norm<data_t>::evaluateImpl(const DataContainer<data_t>& Rx) const
    {
        return Rx.l21MixedNorm();
    }

    template <typename data_t>
    bool MixedL21Norm<data_t>::isProxFriendly() const
    {
        return true;
    }

    template <typename data_t>
    DataContainer<data_t> MixedL21Norm<data_t>::proximal(const DataContainer<data_t>& v,
                                                         SelfType_t<data_t> t) const
    {
        DataContainer<data_t> out(v.getDataDescriptor());
        proximal(v, t, out);
        return out;
    }

    template <typename data_t>
    void MixedL21Norm<data_t>::proximal(const DataContainer<data_t>& v, SelfType_t<data_t> t,
                                        DataContainer<data_t>& out) const
    {
        ProximalMixedL21Norm<data_t> prox;
        prox.apply(v, t, out);
    }

    template <typename data_t>
    void MixedL21Norm<data_t>::getGradientImpl(const DataContainer<data_t>& Rx,
                                               DataContainer<data_t>& out) const
    {
        throw LogicError("MixedL21Norm: not differentiable, so no gradient! (busted!)");
    }
    template <typename data_t>
    LinearOperator<data_t>
        MixedL21Norm<data_t>::getHessianImpl(const DataContainer<data_t>& Rx) const
    {
        throw LogicError("MixedL21Norm: not differentiable, so no hessian! (busted!)");
    }
    template <typename data_t>
    MixedL21Norm<data_t>* MixedL21Norm<data_t>::cloneImpl() const
    {
        return new MixedL21Norm(this->getDomainDescriptor());
    }
    template <typename data_t>
    bool MixedL21Norm<data_t>::isEqual(const Functional<data_t>& other) const
    {
        if (!Functional<data_t>::isEqual(other))
            return false;

        return is<MixedL21Norm>(other);
    }

    // ------------------------------------------
    // explicit template instantiation
    template class MixedL21Norm<float>;
    template class MixedL21Norm<double>;
    // template class MixedL21Norm<complex<float>>;
    // template class MixedL21Norm<complex<double>>;

} // namespace elsa
