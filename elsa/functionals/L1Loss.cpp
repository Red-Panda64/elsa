#include "L1Loss.h"

#include "DataContainer.h"
#include "DataDescriptor.h"
#include "LinearOperator.h"
#include "TypeCasts.hpp"

namespace elsa
{
    template <typename data_t>
    L1Loss<data_t>::L1Loss(const LinearOperator<data_t>& A, const DataContainer<data_t>& b)
        : Functional<data_t>(A.getDomainDescriptor()), A_(A.clone()), b_(b)
    {
    }

    template <typename data_t>
    bool L1Loss<data_t>::isDifferentiable() const
    {
        return true;
    }

    template <typename data_t>
    const LinearOperator<data_t>& L1Loss<data_t>::getOperator() const
    {
        return *A_;
    }

    template <typename data_t>
    const DataContainer<data_t>& L1Loss<data_t>::getDataVector() const
    {
        return b_;
    }

    template <typename data_t>
    data_t L1Loss<data_t>::evaluateImpl(const DataContainer<data_t>& x) const
    {
        auto Ax = A_->apply(x);
        Ax -= b_;

        return Ax.l1Norm();
    }

    template <typename data_t>
    void L1Loss<data_t>::getGradientImpl(const DataContainer<data_t>& x,
                                         DataContainer<data_t>& out) const
    {
        throw LogicError("L1Loss: not differentiable, so no gradient! (busted!)");
    }

    template <typename data_t>
    LinearOperator<data_t> L1Loss<data_t>::getHessianImpl(const DataContainer<data_t>& Rx) const
    {
        throw LogicError("L1Loss: not differentiable, so no Hessian! (busted!)");
    }

    template <typename data_t>
    L1Loss<data_t>* L1Loss<data_t>::cloneImpl() const
    {
        return new L1Loss(*A_, b_);
    }

    template <typename data_t>
    bool L1Loss<data_t>::isEqual(const Functional<data_t>& other) const
    {
        if (!Functional<data_t>::isEqual(other))
            return false;

        auto fn = downcast_safe<L1Loss<data_t>>(&other);
        return fn && *A_ == *fn->A_ && b_ == fn->b_;
    }

    // ------------------------------------------
    // explicit template instantiation
    template class L1Loss<float>;
    template class L1Loss<double>;
} // namespace elsa
